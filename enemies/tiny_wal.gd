extends KinematicBody2D

var state = "idle" #idle, run, jump
var direction = Vector2(0,0)
var speed = 4000
var gravity = Vector2(0, 3)
var alive = true
var on_head = false
var above_friend = null
var on_ground = true

onready var rays_l = [$ray_l, $ray_l2, $ray_l3, $ray_l4, $ray_l5]
onready var rays_r = [$ray_r, $ray_r2, $ray_r3, $ray_r4, $ray_r5]
onready var jump_tween = $jump_Tween
onready var sprite = $Sprite
onready var anim = $AnimationPlayer
onready var player = get_parent().get_node("Player")

func _ready():
	pass
	
func _input(event):
	if alive == true:
		if event.is_action_pressed("jump") && $ground_ray.is_colliding():
			if $ground_ray.get_collider().get_name() == "Player" or "tiny_wal":
				if (self.global_position - player.global_position).length() < 60:
					jump()
			

func _physics_process(delta):
	if alive == true:
		if $dmg_ray.is_colliding() && $dmg_ray.get_collider().get_name() == "Player":
			if $dmg_timer.is_stopped():
				$dmg_timer.start()
		else:
			$dmg_timer.stop()
		
		if $ground_ray.is_colliding() or $ground_ray2.is_colliding():
			on_ground = true
		else:
			on_ground = false
		
		if rays_l[0].is_colliding() or rays_l[1].is_colliding() or rays_l[2].is_colliding() or rays_l[3].is_colliding() or rays_l[4].is_colliding():
			direction = Vector2(-1.5,0)
			state = "run"
			sprite.flip_h = true
		elif rays_r[0].is_colliding() or rays_r[1].is_colliding() or rays_r[2].is_colliding() or rays_r[3].is_colliding() or rays_r[4].is_colliding():
			direction = Vector2(1.5,0)
			state = "run"
			sprite.flip_h = false
		else:
			direction = Vector2(0,0)
			state = "idle"
			if on_ground == true:
				anim.play("idle")
			
		if state == "run":
			if $close_left.is_colliding() or $close_right.is_colliding():
				if on_ground == true:
					jump()
			if on_ground == true:
				if !anim.is_playing():
					anim.play("run")
		
		move_and_slide((direction + gravity) * speed * delta)
	else:
		move_and_slide(Vector2(0,3) * speed * delta)
	


func jump():
	if alive == true:
		if $top_ray.is_colliding():
			if $top_ray.get_collider().get_name() == "tiny_wal":
				$top_ray.get_collider().jump()
		if $top_ray2.is_colliding():
			if $top_ray2.get_collider().get_name() == "tiny_wal":
				$top_ray2.get_collider().jump()
		jump_tween.interpolate_method(self, "jumping", Vector2(0, -5), Vector2(0, 5), .5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		jump_tween.start()
		anim.play("jump")
	

func jumping(value):
	gravity = value




func _on_head_zone_body_entered(body):
	if body.get_name() == "Player":
		body.jump()
		die()

func die():
		$CollisionShape2D3.disabled = true
		alive = false
		anim.play("hit")

func _on_dmg_timer_timeout():
	if $dmg_ray.is_colliding() && $dmg_ray.get_collider().get_name() == "Player":
		$dmg_ray.get_collider().health -= 10
		print($dmg_ray.get_collider().health)
