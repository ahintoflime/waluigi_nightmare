extends KinematicBody2D

var mode = "attack"
var left_or_right = null
var speed = 900
var direction = null
var waluigi_hand = null
var return_speed = 200
var rot_var = null

func _ready():
	if left_or_right == "right":
		direction = Vector2(14,0)
		rot_var = .15
	elif left_or_right == "left":
		direction = Vector2(-14,0)
		rot_var = -.15

func _physics_process(delta):
	if mode == "attack":
		if is_on_wall() == false:
			self.rotation += rot_var
			move_and_slide(direction * speed * delta)
	elif mode == "return":
		var new_dir = (waluigi_hand.global_position - self.global_position).normalized() * 100
		move_and_slide(new_dir * return_speed * delta)


func return_home():
	$CollisionShape2D.disabled = true
	$Area2D/CollisionShape2D.disabled = false
	mode = "return"

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		body.has_racket = true
		queue_free()



func _on_hit_zone_body_entered(body):
		print(body.get_name())
		body.die()
		#return_home()
