extends KinematicBody2D

onready var anim2 = $AnimationPlayer
onready var jump_tween = $misc/jump_tween
onready var ground_ray = [$rays/ground_ray, $rays/ground_ray2, $rays/ground_ray3]
onready var stand_ray = [$rays/stand_ray, $rays/stand_ray2, $rays/stand_ray3]
onready var wal_ray = [$rays/wal_ray, $rays/wal_ray2, $rays/wal_ray3]
onready var sprite = $Sprite
onready var left_ray = $rays/left_ray
onready var right_ray = $rays/right_ray
onready var tennis_racket = preload("res://tennis_racket.tscn")
onready var key_point_l = $misc/key_point_l
onready var key_point_r = $misc/key_point_r
onready var catch_point = $misc/catch_point

#move states walk, run, bash, crouch
var health = 100
var move_state = "walk"
var ground_collision = false
var running = false
var crouching = false
var able_to_stand = true
var left_or_right = "right"
var has_racket = true
var speed = 100
var move_dir = Vector2(0,0)
var gravity = Vector2(0, 150)
var walk_vector = Vector2(60, 0)
var run_vector = Vector2(80, 0)
var crouch_vector = Vector2(30, 0)
var world_racket = null
var front_clear = true

var crouchmod = 1

func _input(event):
	
	if event.is_action_pressed("sprint") && crouching == false:
		running = true
	if event.is_action_released("sprint"):
		running = false
	
	if event.is_action_pressed("jump") && ground_collision == true:
		jump()

	if event.is_action_pressed("throw_racket") && has_racket == true:
		if front_clear == true:
			world_racket = tennis_racket.instance()
			world_racket.left_or_right = left_or_right
			if left_or_right == "right":
				if crouching == true:
					world_racket.global_position = $misc/throw_point_r2.global_position
				else:
					world_racket.global_position = $misc/throw_point_r.global_position
			elif left_or_right == "left":
				if crouching == true:
					world_racket.global_position = $misc/throw_point_l2.global_position
				else:
					world_racket.global_position = $misc/throw_point_l.global_position
			left_ray.add_exception(world_racket)
			right_ray.add_exception(world_racket)
			for ray in stand_ray:
				ray.add_exception(world_racket)
			get_parent().add_child(world_racket)
			has_racket = false
			
	elif event.is_action_pressed("throw_racket") && has_racket == false:
		world_racket.waluigi_hand = $misc/catch_point
		world_racket.return_home()
		
func _ready(): 
	OS.window_size = OS.window_size * 2

	#rays exclude player kinematicbody
	for ray in ground_ray:
		ray.add_exception(self)
	for ray in stand_ray:
		ray.add_exception(self)
	left_ray.add_exception(self)
	right_ray.add_exception(self)
	

func _physics_process(delta):
	#detects if the space in front of Waluigi is clear
	if left_or_right == "left" && !left_ray.is_colliding():
		front_clear = true
	elif left_or_right == "right" && !right_ray.is_colliding():
		front_clear = true
	else:
		front_clear = false
	
	#detects if waluigi is able to stand up from crouching
	if stand_ray[0].is_colliding() or stand_ray[1].is_colliding() or stand_ray[2].is_colliding():
		able_to_stand = false
	else:
		able_to_stand = true
		
	#ground detection
	if ground_ray[0].is_colliding() or ground_ray[1].is_colliding() or ground_ray[2].is_colliding():
		ground_collision = true
	else:
		ground_collision = false
	
	#jumping animations
	if ground_collision == false:
		if crouching == true:
			pass
		elif gravity.y < 150:
			if running == true:
				anim2.stop()
				anim2.play("run_jump")
			else:
				anim2.stop()
				anim2.play("jump")
		else:
			if running == false:
				anim2.stop()
				anim2.play("fall")

	
	
	#flips sprite left or right
	if left_or_right == "right" && sprite.flip_h == true:
		sprite.flip_h = false
	if left_or_right == "left" && sprite.flip_h == false:
		sprite.flip_h = true
		

	#movement code!
	if Input.is_action_pressed("ui_right"):
		left_or_right = "right"
		if running == true:
			if ground_collision == true:
				if anim2.current_animation != "run":
					anim2.stop()
					anim2.play("run")
			move_dir = run_vector
		elif crouching == true:
			if ground_collision == true:
				if anim2.current_animation != "crouch_walk":
					anim2.stop()
					anim2.play("crouch_walk")
			move_dir = crouch_vector
		else:
			move_dir = walk_vector
			if ground_collision == true:
				if anim2.current_animation != "walk":
					anim2.stop()
					anim2.play("walk")
	elif Input.is_action_pressed("ui_left"):
		left_or_right = "left"
		if running == true && crouching == false:
			if ground_collision == true:
				if anim2.current_animation != "run":
					anim2.stop()
					anim2.play("run")
			move_dir = -run_vector
		elif crouching == true:
			if ground_collision == true:
				if anim2.current_animation != "crouch_walk":
					anim2.stop()
					anim2.play("crouch_walk")
			move_dir = -crouch_vector
		else:
			move_dir = -walk_vector
			if ground_collision == true:
				if anim2.current_animation != "walk":
					anim2.stop()
					anim2.play("walk")
	else:
		move_dir = Vector2(0,0)
		if ground_collision == true:
			if crouching == true:
				anim2.stop()
				anim2.play("crouch")
			else:
				anim2.stop()
				anim2.play("idle")
	
	# crouching code! :)
	if crouching == true:
		$collision_normal.disabled = true
		$collision_crouch.disabled = false
	if Input.is_action_pressed("ui_down"):
		crouching = true
	elif able_to_stand == true:
		crouching = false
		$collision_normal.disabled = false
		$collision_crouch.disabled = true

	
	move_and_slide((move_dir + gravity) * speed * delta)

func jump():
	if running == true && move_dir != Vector2(0,0):
		jump_tween.interpolate_method(self, "jumping", Vector2(0, -280), Vector2(0, 150), .66, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		jump_tween.start()
	else:
		jump_tween.interpolate_method(self, "jumping", Vector2(0, -260), Vector2(0, 150), .5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		jump_tween.start()

func jumping(value):
	gravity = value
