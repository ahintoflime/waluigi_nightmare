extends Control

onready var chat_box = get_parent().get_node("lil_key_guy/chat_box")


var test_script_1 = ["This is the first line", "This is the second line", "and THIS is the third and BEST line!"]
var test_script_2 = ["You can hold DOWN to crouch under obstacles"]

func _ready():

	pass


func _on_text_trigger_1_body_entered(body):
	if body.get_name() == "Player":
		chat_box.pop_up(test_script_2)
		$text_trigger_1.queue_free()
