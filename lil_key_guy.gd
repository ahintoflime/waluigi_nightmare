extends KinematicBody2D

onready var waluigi = get_parent().get_node("Player")
onready var sprite = $lil_guy
onready var mouth = $big_mouth
var current_target = null
var dir = null
var dir_to_move = null
var speed = 5100
var orientation = "right"

func _ready():
	pass
		
func _physics_process(delta):
	if waluigi.left_or_right == "right":
		current_target = waluigi.key_point_l.global_position

	elif waluigi.left_or_right == "left":
		current_target = waluigi.key_point_r.global_position

	dir = Vector2(current_target - self.global_position)
	dir_to_move = dir.normalized()
	if dir.length() > 1:
		move_and_slide(dir_to_move * speed * delta)
	
	#buddy speeds up if he gets too far
	if dir.length() > 200:
		speed = 6900
	elif dir.length() > 110:
		speed = 6100
	else:
		speed = 5100
	
	#print(speed)
	
	if dir.x > .5:
		orientation = "right"
	elif dir.x < -.5:
		orientation = "left"
	
	if orientation == "right":
		$big_mouth2.hide()
		$big_mouth.show()
		sprite.flip_h = false
	elif orientation == "left":
		$big_mouth2.show()
		$big_mouth.hide()
		sprite.flip_h = true