extends Control

onready var text = $Label
onready var bg = $text_bg

var current_script = null
var line_count = 0


func _input(event):
	if event.is_action_pressed("next") && self.visible == true:
		next_line()

func _ready():
	pass


func next_line():
	if current_script == null:
		self.visible = false
		line_count = 0
	else:
		text.text = current_script[line_count]
		line_count += 1
		if line_count == (current_script.size()):
			current_script = null

func pop_up(script):
	self.visible = true
	current_script = script
	next_line()